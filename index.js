const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const user = require('./user')
const app = express()

mongoose.connect('mongodb://localhost:27017/awstestapp', { useNewUrlParser: true, useUnifiedTopology: true });
app.use(cors())
app.use(express.json());

app.post('/', async (req, res) => {
    try {
        await user.create(req.body)
        res.send("done")
    } catch (error) {
        res.status(500).json({
            error: error
        })
    }
})

app.get('/', async (req, res) => {
    try {
        const data = await user.find({})
        res.send(data)
    } catch (error) {
        res.status(500).json({
            error: error
        })
    }
})

app.listen(4000, () => {
    console.log("server listening")
})
